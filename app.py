# -*- coding: utf-8 -*-
import os

from flask import Flask, render_template, request, redirect, url_for, session
from flask_login import (LoginManager, UserMixin, login_required, login_user, logout_user, current_user)
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api, Resource
from flask_marshmallow import Marshmallow
import bcrypt
import marshmallow_sqlalchemy
import pymysql
import flask_pymysql

# Configuration de l'application
app = Flask(__name__)
app.config["DEBUG"] = True
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://root:@localhost/ufweb?charset=utf8mb4"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECRET_KEY'] = 'mySecretKey'
app.config['SECURITY_PASSWORD_SALT'] = 'myPassword'
app.config['USE_SESSION_FOR_NEXT'] = True

# On instancie l'api et la base de données
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'connexion'
login_manager.login_message = 'Vous devez vous connecter'

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

roles_users = db.Table('roles_users',
                       db.Column('id_user', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('id_role', db.Integer(), db.ForeignKey('role.id_role')))

# Rentrer les données dans l'api qui est stockée dans la base de données
class Jeux(db.Model):

   id_jeux = db.Column(db.Integer, primary_key=True)
   nom = db.Column(db.String(100))
   prix = db.Column(db.Integer)
   note = db.Column(db.Integer)
   nombre_achat = db.Column(db.String(100))
   image = db.Column(db.String(200))

   def __init__(self, nom, prix, note, image, nombre_achat):
      self.nom = nom
      self.prix = prix
      self.note = note
      self.image = image
      self.nombre_achat = nombre_achat

   def __repr__(self):
      return "<Nom: {}>".format(self.nom), "<Prix: {}>".format(self.prix), "<Note: {}>".format(
         self.note), "<Nombre_achat: {}>".format(
         self.nombre_achat), "<Image: {}>".format(self.image)

class JeuxSchema(ma.Schema):
   class Meta:
      fields = ("id_jeux", "nom", "prix", "note", "image", "nombre_achat")

jeux_schema = JeuxSchema()
jeux_schemas = JeuxSchema(many=True)

panier_jeux = db.Table('panier_jeux',
                        db.Column('jeux_id_jeux', db.Integer(), db.ForeignKey('jeux.id_jeux')),
                        db.Column('panier_id_panier', db.Integer(), db.ForeignKey('panier.id_panier')))

class Role(db.Model):
   id_role = db.Column(db.Integer(), primary_key=True)
   name = db.Column(db.String(80), unique=True)
   description = db.Column(db.String(255))

class User(db.Model, UserMixin):

   id = db.Column(db.Integer, primary_key=True)
   nom = db.Column(db.String(100))
   prenom = db.Column(db.String(100))
   age = db.Column(db.Integer)
   email = db.Column(db.String(200))
   password = db.Column(db.String(255))
   date_naissance = db.Column(db.String(100))
   solde = db.Column(db.Integer)
   

   def __init__(self, nom, prenom, age, email, password, date_naissance, solde):
      self.nom = nom
      self.prenom = prenom
      self.age = age
      self.email = email
      self.password = password
      self.date_naissance = date_naissance
      self.solde = solde

   def __repr__(self):
      return "<Nom: {}>".format(self.nom), "<Prenom: {}>".format(
         self.prenom), "<Age: {}>".format(self.age), "<email: {}>".format(
         self.email), "<Password: {}>".format(self.password), "<Date_naissance: {}>".format(
         self.date_naissance), "<Solde: {}>".format(
         self.solde)
class UserSchema(ma.Schema):
   class Meta:
      fields = ("id", "nom", "prenom", "age", "email", "password", "date_naissance", "solde")

user_schema = UserSchema()
user_schemas = UserSchema(many=True)

user_jeux = db.Table('user_jeux',
                        db.Column('jeux_id_jeux', db.Integer(), db.ForeignKey('jeux.id_jeux')),
                        db.Column('user_id_user', db.Integer(), db.ForeignKey('user.id')))

class Panier(db.Model):

   id_panier = db.Column(db.Integer, primary_key=True)
   prix_total = db.Column(db.String(100))
   id_user = db.Column(db.Integer, db.ForeignKey('user.id'))

   def __init__(self, prix_total, id_user):
      self.prix_total = prix_total
      self.id_user = id_user

   def __repr__(self):
      return "<Prix_total: {}>".format(
         self.prix_total), "<Id_user: {}>".format(self.id_user)
   
class PanierSchema(ma.Schema):
   class Meta:
      fields = ("id_panier", "prix_total", "id_user")

panier_schema = PanierSchema()
panier_schemas = PanierSchema(many=True)

class Avis(db.Model):

   id_avis = db.Column(db.Integer, primary_key=True)
   note = db.Column(db.String(100))
   texte = db.Column(db.String(100))
   date = db.Column(db.String(100))
   id_jeux = db.Column(db.Integer, db.ForeignKey('jeux.id_jeux'))
   id_user = db.Column(db.Integer, db.ForeignKey('user.id'))

   def __init__(self, note, texte, date, id_jeux, id_user):
      self.note = note
      self.texte = texte
      self.date = date
      self.id_jeux = id_jeux
      self.id_user =id_user

   def __repr__(self):
      return "<Note: {}>".format(self.note), "<Texte: {}>".format(
         self.texte), "<Date: {}>".format(
         self.date), "<Id_jeux: {}>".format(self.id_jeux), "<Id_User: {}>".format(self.id_user)   

class AvisSchema(ma.Schema):
   class Meta:
      fields = ("id_avis", "note", "texte", "date", "id_jeux", "id_user")

avis_schema = AvisSchema()
avis_schemas = AvisSchema(many=True)

class JeuxListResource(Resource):
   def get(self):
      jeux = Jeux.query.all()
      return jeux_schema.dump(Jeux)

   def post(self):
      new_post = Jeux(
         nom=request.json['nom'],
         prix=request.json['prix'],
         note=request.json['note'],
         image=request.json['image'],
         nombre_achat=request.json['nombre_achat']
      )
      db.session.add(new_post)
      db.session.commit()
      return jeux_schema.dump(new_post)


api.add_resource(JeuxListResource, '/jeux')


class JeuxResource(Resource):
   def get(self, id_jeux):
      jeux = Jeux.query.get_or_404(id_jeux)
      return jeux_schema.dump(jeux)

   def patch(self, id_jeux):
      jeux = Jeux.query.get_or_404(id_jeux)
      if 'nom' in request.json:
         jeux.nom = request.json['nom']
      if 'prix' in request.json:
         jeux.prix = request.json['prix']
      if 'note' in request.json:
         jeux.note = request.json['note']
      if 'image' in request.json:
         jeux.image = request.json['image']
      if 'nombre_achat' in request.json:
         jeux.nombre_achat = request.json['nombre_achat']

      db.session.commit()
      return jeux_schema.dump(jeux)

   def delete(self, id_jeux):
      jeux = Jeux.query.get_or_404(id_jeux)
      db.session.delete(jeux)
      db.session.commit()
      return '', 204

api.add_resource(JeuxResource, '/jeux/<int:jeux_id>')


class PanierListResource(Resource):
   def get(self):
      panier = Panier.query.all()
      return panier_schema.dump(Panier)

   def post(self):
      new_post = Panier(
         prix_total=request.json['prix_total'],
         id_user=request.json['id_user']
      )
      db.session.add(new_post)
      db.session.commit()
      return panier_schema.dump(new_post)


api.add_resource(PanierListResource, '/panier')

class PanierResource(Resource):
   def get(self, id_panier):
      panier = Panier.query.get_or_404(id_panier)
      return panier_schema.dump(panier)

   def patch(self, id_panier):
      panier = Panier.query.get_or_404(id_panier)
      if 'prix_total' in request.json:
         panier.prix_total = request.json['prix_total']

      db.session.commit()
      return panier_schema.dump(panier)

   def delete(self, id_panier):
      panier = Panier.query.get_or_404(id_panier)
      db.session.delete(panier)
      db.session.commit()
      return '', 204

api.add_resource(PanierResource, '/panier/<int:id_panier>')


class UserListResource(Resource):
   def get(self):
      user = User.query.all()
      return user_schema.dump(user)

   def post(self):
      new_post = User(
         nom=request.json['nom'],
         prenom=request.json['prenom'],
         age=request.json['age'],
         email=request.json['email'],
         password=request.json['password'],
         date_naissance=request.json['date_naissance'],
         solde=request.json['solde']
      )
      db.session.add(new_post)
      db.session.commit()
      return user_schema.dump(new_post)


api.add_resource(UserListResource, '/user')

class UserResource(Resource):
   def get(self, id):
      user = User.query.get_or_404(id)
      return user_schema.dump(user)

   def patch(self, id):
      user = User.query.get_or_404(id)
      if 'nom' in user.json:
         user.nom = user.json['nom']
      if 'prenom' in user.json:
         user.prenom = user.json['prenom']
      if 'age' in user.json:
         user.age = user.json['age']
      if 'email' in user.json:
         user.email = user.json['email']
      if 'password' in user.json:
         user.password = user.json['password']
      if 'date_naissance' in user.json:
         user.date_naissance = user.json['date_naissance']
      if 'solde' in user.json:
         user.solde = user.json['solde']

      db.session.commit()
      return user_schema.dump(user)

   def delete(self, id):
      user = User.query.get_or_404(id)
      db.session.delete(user)
      db.session.commit()
      return '', 204

api.add_resource(UserResource, '/user/<int:id>')


class AvisListResource(Resource):
   def get(self):
      avis = Avis.query.all()
      return avis_schema.dump(avis)

   def post(self):
      new_post = Avis(
         note=request.json['note'],
         texte=request.json['texte'],
         date=request.json['date'],
         id_jeux=request.json['id_jeux'],
         id_user=request.json['id_user']
      )
      db.session.add(new_post)
      db.session.commit()
      return avis_schema.dump(new_post)


api.add_resource(AvisListResource, '/avis')

class AvisResource(Resource):
   def get(self, id_avis):
      avis = Avis.query.get_or_404(id_avis)
      return avis_schema.dump(avis)

   def patch(self, id_avis):
      avis = Avis.query.get_or_404(id_avis)
      if 'note' in avis.json:
         avis.note = avis.json['note']
      if 'texte' in avis.json:
         avis.texte = avis.json['texte']
      if 'date' in avis.json:
         avis.date = avis.json['date']

      db.session.commit()
      return avis_schema.dump(avis)

   def delete(self, id_avis):
      avis = Avis.query.get_or_404(id_avis)
      db.session.delete(avis)
      db.session.commit()
      return '', 204

api.add_resource(AvisResource, '/avis/<int:id_avis>')

@app.route('/', methods=['GET'])
def index():
   jeux = Jeux.query.all()
   return render_template('index.html', jeux = jeux)

@app.route('/profile/<email>')
@login_required
def profile(email):
   user = User.query.filter_by(email=email).first()
   return render_template('profile.html', user=user)

@app.route('/jeux/<nom>', methods=['GET'])
@login_required
def create_get(nom):
   jeux = Jeux.query.filter_by(nom=nom).first()
   return render_template('jeux.html', jeux = jeux)

@app.route('/create_jeuxPost', methods=['POST'])
def create_post():
   jeu = Jeux(request.form['nom'],
               request.form['prix'], 
               request.form['note'],
               request.form['nombre_achat'], 
               request.form['image'])
   db.session.add(jeu)
   db.session.commit()
   return redirect(url_for('index'))

@app.route('/update-deleteJeux', methods=['GET', 'POST'])
@login_required
def upadte_delete():
   if request.form:
      jeu = Jeux(nom=request.form.get("nom"), prix=request.form.get("prix"), note=request.form.get("note"),
                   nombre_achat=request.form.get("nombre_achat"), image=request.form.get("image"))
      db.session.add(jeu)
      db.session.commit()
   jeux = Jeux.query.all()
   return render_template("updateDelete.html", jeux=jeux)


@app.route('/update_Jeux', methods=['POST'])
@login_required
def update():
   newnom = request.form.get("newnom")
   oldnom = request.form.get("oldnom")
   newprix = request.form.get("newprix")
   oldprix = request.form.get("oldprix")
   newnote = request.form.get("newnote")
   oldnote = request.form.get("oldnote")
   newnombre_achat = request.form.get("newnombre_achat")
   oldnombre_achat = request.form.get("oldnombre_achat")
   newimage = request.form.get("newimage")
   oldimage = request.form.get("oldimage")
   jeu = Jeux.query.filter_by(nom = oldnom, prix = oldprix, note = oldnote, nombre_achat = oldnombre_achat, image = oldimage)
   jeu.nom = newnom
   jeu.prix = newprix
   jeu.note = newnote
   jeu.nombre_achat = newnombre_achat
   jeu.image = newimage
   db.session.commit()
   return redirect("/")

@app.route('/delete_jeux', methods=['POST'])
@login_required
def delete():
   nom = request.form.get("nom")
   prix = request.form.get("prix")
   note = request.form.get("note")
   nombre_achat = request.form.get("nombre_achat")
   image = request.form.get("image")
   jeu = Jeux.query.filter_by(nom = nom, prix = prix, note = note, nombre_achat = nombre_achat, image = image).first()
   db.session.delete(jeu)
   db.session.commit()
   return redirect("/")

@app.route('/create_avisPost', methods=['POST'])
@login_required
def create_postAvis():
   avi = Avis(request.form['note'], 
               request.form['texte'], 
               request.form['date'],
               request.form['id_jeux'],
               request.json['id_user'])
   db.session.add(avi)
   db.session.commit()
   return redirect(url_for('index'))

@app.route('/update_avis', methods=['POST'])
@login_required
def updateAvis():
   newnote = request.form.get("newnote")
   oldnote = request.form.get("oldnote")
   newtexte = request.form.get("newtexte")
   oldtexte = request.form.get("oldtexte")
   newdate = request.form.get("newdate")
   olddate = request.form.get("olddate")
   avi = Avis.query.filter_by(note = oldnote, texte = oldtexte, date = olddate)
   avi.note = newnote
   avi.texte = newtexte
   avi.date = newdate
   db.session.commit()
   return redirect("/")

@app.route('/delete_avis', methods=['POST'])
@login_required
def deleteAvis():
   note = request.form.get("note")
   texte = request.form.get("texte")
   date = request.form.get("date")
   avi = Avis.query.filter_by(note = note, texte = texte, date = date).first()
   db.session.delete(avi)
   db.session.commit()
   return redirect("/")
   
@app.route('/create_userGet', methods=['GET'])
def create_getuser():
   return render_template('register.html')

@app.route('/create_userPost', methods=['POST'])
def create_postuser():
   user = User(request.form['nom'], 
               request.form['prenom'],
               request.form['age'], 
               request.form['email'],
               request.form['password'], 
               request.form['date_naissance'],
               request.form['solde'])
   db.session.add(user)
   db.session.commit()
   return redirect(url_for('index'))

@app.route('/update_deleteUser/<email>', methods=['GET', 'POST'])
@login_required
def upadte_deleteUser(email):
   user = User.query.filter_by(email=email).first()
   return render_template("updateUser.html", user=user)

@app.route('/update_user', methods=['POST'])
@login_required
def updateUser():
   newnom = request.form.get("newnom")
   oldnom = request.form.get("oldnom")
   newprenom = request.form.get("newprenom")
   oldprenom = request.form.get("oldprenom")
   newage = request.form.get("newage")
   oldage = request.form.get("oldage")
   newemail = request.form.get("nemail")
   oldemail = request.form.get("oldemail")
   newpassword = request.form.get("newpassword")
   oldpassword = request.form.get("oldpassword")
   newdate_naissance = request.form.get("newdate_naissance")
   olddate_naissance = request.form.get("olddate_naissance")
   user = Avis.query.filter_by(nom = oldnom, prenom = oldprenom, age = oldage, email = oldemail, password = oldpassword, date_naissance = olddate_naissance)
   user.nom = newnom
   user.prenom = newprenom
   user.age = newage
   user.email = newemail
   user.password = newpassword
   user.date_naissance = newdate_naissance
   db.session.commit()
   return redirect("/")

@app.route('/connexion')
def connexion():
   return render_template('connexion.html')

@app.route('/connectmoi', methods=['POST'])
def connectmoi():
   email = request.form['email']
   password = request.form['password']

   user1 = User.query.filter_by(email=email).first()
   user2 = User.query.filter_by(password=password).first()
   
   if not user1:
      return render_template('connexion.html')

   if not user2:
      return render_template('connexion.html')

   if(user1 == user2):
      login_user(user1)
      return redirect("/")

@app.route('/deconnect')
@login_required
def deconnect():
   logout_user()
   return redirect("/")


if __name__ == '__main__':
   app.run(debug=True)
